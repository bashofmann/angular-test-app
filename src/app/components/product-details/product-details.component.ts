import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product, products } from '../../products';
import { CartService } from '../../services/cart.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
})
export class ProductDetailsComponent implements OnInit {
  product!: Product;

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService,
    private _notificationService: NotificationsService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.product = products[+params.get('productId')!];
    });
  }

  addToCart(product: Product) {
    this._notificationService.success(
      'Your product has been added to the cart!'
    );
    this.cartService.addToCart(product);
  }
}
