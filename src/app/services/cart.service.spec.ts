import { TestBed } from '@angular/core/testing';

import { CartService } from './cart.service';
import { HttpClientModule } from '@angular/common/http';

describe('CartService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    })
  );

  it('should be created', () => {
    const service: CartService = TestBed.get(CartService);
    expect(service).toBeTruthy();
  });

  it('cart functionality', () => {
    const service: CartService = TestBed.get(CartService);

    expect(service.getItems()).toEqual([]);

    const product = {
      name: 'test',
      description: 'description',
      price: 10,
    };
    service.addToCart(product);

    expect(service.getItems()).toEqual([product]);

    service.clearCart();

    expect(service.getItems()).toEqual([]);
  });
});
